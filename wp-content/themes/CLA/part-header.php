<?php $logo = ''; ?>
<?php if ( is_page( array( 'sidney' ) ) ) : $logo = '_sidney'; $class = 'sidney'; endif; ?>
<?php if ( is_page( array( 'melbourne' ) ) ) : $logo = '_melbourne'; $class = 'melbourne'; endif; ?>
<?php if ( is_page( array( 'brisbane' ) ) ) : $logo = '_brisbane'; $class = 'brisbane'; endif; ?>
<?php if ( is_page( array( 'perth' ) ) ) : $logo = '_perth'; $class = 'perth'; endif; ?>
<?php if ( is_page( array( 'adelaide' ) ) ) : $logo = '_adelaide'; $class = 'adelaide'; endif; ?>
<?php if ( is_page( array( 'gold-coast' ) ) ) : $logo = '_gold_coast'; $class = 'gold_coast'; endif; ?>
<?php if ( is_page( array( 'canberra' ) ) ) : $logo = '_canberra'; $class = 'canberra'; endif; ?>
<?php if ( is_page( array( 'darwin' ) ) ) : $logo = '_darwin'; $class = 'darwin'; endif; ?>
<?php if ( is_page( array( 'cairns' ) ) ) : $logo = '_cairns'; $class = 'cairns'; endif; ?>
<?php if ( is_page( array( 'hobart' ) ) ) : $logo = '_hobart'; $class = 'hobart'; endif; ?>
<!-- Begin Top -->
	<section class="top <?php echo $class; ?> wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'logo' . $logo ); ?>
			</div>
			<div class="small-12 medium-6 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( 'log_in' ); ?>
			</div>
			<div class="small-12 medium-1 columns">
				<?php dynamic_sidebar( 'translate' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->