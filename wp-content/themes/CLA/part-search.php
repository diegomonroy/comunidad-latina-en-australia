<?php if ( is_front_page() ) : $class = 'home'; endif; ?>
<?php if ( is_page( array( 'sidney' ) ) ) : $class = 'sidney'; endif; ?>
<?php if ( is_page( array( 'melbourne' ) ) ) : $class = 'melbourne'; endif; ?>
<?php if ( is_page( array( 'brisbane' ) ) ) : $class = 'brisbane'; endif; ?>
<?php if ( is_page( array( 'perth' ) ) ) : $class = 'perth'; endif; ?>
<?php if ( is_page( array( 'adelaide' ) ) ) : $class = 'adelaide'; endif; ?>
<?php if ( is_page( array( 'gold-coast' ) ) ) : $class = 'gold_coast'; endif; ?>
<?php if ( is_page( array( 'canberra' ) ) ) : $class = 'canberra'; endif; ?>
<?php if ( is_page( array( 'darwin' ) ) ) : $class = 'darwin'; endif; ?>
<?php if ( is_page( array( 'cairns' ) ) ) : $class = 'cairns'; endif; ?>
<?php if ( is_page( array( 'hobart' ) ) ) : $class = 'hobart'; endif; ?>
<!-- Begin Search -->
	<section class="search <?php echo $class; ?> wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'search_home' ); endif; ?>
				<?php if ( is_page( array( 'sidney' ) ) ) : dynamic_sidebar( 'search_sidney' ); endif; ?>
				<?php if ( is_page( array( 'melbourne' ) ) ) : dynamic_sidebar( 'search_melbourne' ); endif; ?>
				<?php if ( is_page( array( 'brisbane' ) ) ) : dynamic_sidebar( 'search_brisbane' ); endif; ?>
				<?php if ( is_page( array( 'perth' ) ) ) : dynamic_sidebar( 'search_perth' ); endif; ?>
				<?php if ( is_page( array( 'adelaide' ) ) ) : dynamic_sidebar( 'search_adelaide' ); endif; ?>
				<?php if ( is_page( array( 'gold-coast' ) ) ) : dynamic_sidebar( 'search_gold_coast' ); endif; ?>
				<?php if ( is_page( array( 'canberra' ) ) ) : dynamic_sidebar( 'search_canberra' ); endif; ?>
				<?php if ( is_page( array( 'darwin' ) ) ) : dynamic_sidebar( 'search_darwin' ); endif; ?>
				<?php if ( is_page( array( 'cairns' ) ) ) : dynamic_sidebar( 'search_cairns' ); endif; ?>
				<?php if ( is_page( array( 'hobart' ) ) ) : dynamic_sidebar( 'search_hobart' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Search -->