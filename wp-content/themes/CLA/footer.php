		<?php get_template_part( 'part', 'block-1' ); ?>
		<?php get_template_part( 'part', 'newsletter' ); ?>
		<?php get_template_part( 'part', 'social-media' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php if ( ! is_front_page() ) : get_template_part( 'part', 'back-home' ); endif; ?>
		<?php wp_footer(); ?>
	</body>
</html>