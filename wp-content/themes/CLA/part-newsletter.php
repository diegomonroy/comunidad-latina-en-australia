<!-- Begin Newsletter -->
	<section class="newsletter wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'newsletter' ); ?>
			</div>
		</div>
	</section>
<!-- End Newsletter -->