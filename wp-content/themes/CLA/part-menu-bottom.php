<!-- Begin Menu Bottom -->
	<div class="moduletable_bo1">
		<?php
		wp_nav_menu(
			array(
				'menu_class' => 'menu align-center',
				'container' => false,
				'theme_location' => 'bottom-menu',
				'items_wrap' => '<ul class="%2$s">%3$s</ul>'
			)
		);
		?>
	</div>
<!-- End Menu Bottom -->