<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/bower_components/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/bower_components/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/bower_components/jquery/dist/jquery.min.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/bower_components/what-input/dist/what-input.min.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/bower_components/wow/dist/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'bottom-menu' => __( 'Bottom Menu' ),
			'main-menu' => __( 'Main Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Sidney',
			'id' => 'logo_sidney',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Melbourne',
			'id' => 'logo_melbourne',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Brisbane',
			'id' => 'logo_brisbane',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Perth',
			'id' => 'logo_perth',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Adelaide',
			'id' => 'logo_adelaide',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Gold Coast',
			'id' => 'logo_gold_coast',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Canberra',
			'id' => 'logo_canberra',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Darwin',
			'id' => 'logo_darwin',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Cairns',
			'id' => 'logo_cairns',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo Hobart',
			'id' => 'logo_hobart',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Log In',
			'id' => 'log_in',
			'before_widget' => '<div class="moduletable_to3 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Translate',
			'id' => 'translate',
			'before_widget' => '<div class="moduletable_to4 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Home',
			'id' => 'search_home',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Sidney',
			'id' => 'search_sidney',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Melbourne',
			'id' => 'search_melbourne',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Brisbane',
			'id' => 'search_brisbane',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Perth',
			'id' => 'search_perth',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Adelaide',
			'id' => 'search_adelaide',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Gold Coast',
			'id' => 'search_gold_coast',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Canberra',
			'id' => 'search_canberra',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Darwin',
			'id' => 'search_darwin',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Cairns',
			'id' => 'search_cairns',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Hobart',
			'id' => 'search_hobart',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Items Home',
			'id' => 'items_home',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'News Home',
			'id' => 'news_home',
			'before_widget' => '<div class="moduletable_b12">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Ads Home',
			'id' => 'ads_home',
			'before_widget' => '<div class="moduletable_b13">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Newsletter',
			'id' => 'newsletter',
			'before_widget' => '<div class="row align-center align-middle moduletable_n1">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Social Media',
			'id' => 'social_media',
			'before_widget' => '<div class="moduletable_sm1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo2">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Custom shortcode to Log In
 */
function shortcode_log_in() {
	if ( is_user_logged_in() ) {
		$link = '';
		$text = 'Cierra Sesión';
	} else {
		$link = '';
		$text = 'Inicia Sesión';
	}
	$html = '<a href="' . $link . '"><img src="http://dev02.amapolazul.com/cla/wp-content/themes/CLA/build/icon_log_in.png"> <img src="http://dev02.amapolazul.com/cla/wp-content/themes/CLA/build/icon_separator.png"> ' . $text . '</a>';
	return $html;
}
add_shortcode( 'log_in', 'shortcode_log_in' );

/*
 * Custom shortcode to Search Home
 */
function shortcode_search_home() {
	$html = '
		<h3 class="text-center">¿QUÉ ESTÁS BUSCANDO?</h3>
		<div class="search_container">
			<div class="row align-center align-middle">
				<div class="small-12 medium-4 columns">
					<select>
						<option value="0">Ciudad</option>
						<option value="sidney">Sidney</option>
						<option value="melbourne">Melbourne</option>
						<option value="brisbane">Brisbane</option>
						<option value="perth">Perth</option>
						<option value="adelaide">Adelaide</option>
						<option value="gold_coast">Gold Coast</option>
						<option value="canberra">Canberra</option>
						<option value="darwin">Darwin</option>
						<option value="cairns">Cairns</option>
						<option value="hobart">Hobart</option>
					</select>
				</div>
				<div class="small-12 medium-8 columns">
					<div class="input-group">
						<input type="text" class="input-group-field" placeholder="¿Qué buscas?">
						<div class="input-group-button">
							<button type="submit" class="button"><i class="fa fa-search" aria-hidden="true"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	';
	return $html;
}
add_shortcode( 'search_home', 'shortcode_search_home' );

/*
 * Custom shortcode to Main Categories
 */
function shortcode_main_categories( $atts ) {
	extract( shortcode_atts( array( 'city' => 'sidney' ), $atts ) );
	$web = get_site_url() . '/' . $city . '/';
	$categories = array(
		array( 'Asesorías', 'asesorias', 'asesorias' ),
		array( 'Belleza y Cuidado del Cuerpo', 'belleza-y-cuidado-del-cuerpo', 'belleza_y_cuidado_del_cuerpo' ),
		array( 'Servicios para Hogar', 'servicios-para-hogar', 'servicios_para_hogar' ),
		array( 'Educación', 'educacion', 'educacion' ),
		array( 'Vehículos', 'vehiculos', 'vehiculos' ),
		array( 'Motos', 'motos', 'motos' ),
		array( 'Bicicletas', 'bicicletas', 'bicicletas' ),
		array( 'Alimentos y Bebidas', 'alimentos-y-bebidas', 'alimentos_y_bebidas' ),
		array( 'Alojamiento', 'alojamiento', 'alojamiento' ),
		array( 'Marketing Publicidad y Diseño Gráfico', 'marketing-publicidad-y-diseno-grafico', 'marketing_publicidad_y_diseno_grafico' ),
		array( 'Video y Fotografía', 'video-y-fotografia', 'video_y_fotografia' ),
		array( 'Deportes y Recreación', 'deportes-y-recreacion', 'deportes_y_recreacion' ),
		array( 'Salud', 'salud', 'salud' ),
		array( 'Trabajo', 'trabajo', 'trabajo' ),
		array( 'Transporte y Logística', 'transporte-y-logistica', 'transporte_y_logistica' ),
		array( 'Diversión', 'diversion', 'diversion' ),
		array( 'Turismo y Viajes', 'turismo-y-viajes', 'turismo_y_viajes' ),
		array( 'Ropa Zapatos y Accesorios', 'ropa-zapatos-y-accesorios', 'ropa_zapatos_y_accesorios' ),
		array( 'Artesanías y Suvenires', 'artesanias-y-suvenires', 'artesanias_y_suvenires' ),
		array( 'Mascotas', 'mascotas', 'mascotas' ),
		array( 'Cultura, Arte y Religión', 'cultura-arte-y-religion', 'cultura_arte_y_religion' ),
		array( 'Regalos', 'regalos', 'regalos' ),
		array( 'Bancos y Cambios de Moneda', 'bancos-y-cambios-de-moneda', 'bancos_y_cambios_de_moneda' )
	);
	$count = count( $categories );
	$html = '<div id="main_categories" class="row align-center align-top">';
	for ( $i = 0; $i <= $count; $i++ ) {
		$link = $web . $categories[$i][1];
		$html .= '
			<div class="small-12 medium-2 columns text-center ' . $categories[$i][2] . '">
				<a href="' . $link . '" class="image"></a>
				<a href="' . $link . '" class="text">' . $categories[$i][0] . '</a>
			</div>
		';
	}
	$html .= '</div>';
	return $html;
}
add_shortcode( 'main_categories', 'shortcode_main_categories' );