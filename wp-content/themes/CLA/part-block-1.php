<!-- Begin Block 1 -->
	<section class="block_1 wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'items_home' ); ?>
				<?php dynamic_sidebar( 'news_home' ); ?>
			</div>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'ads_home' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 1 -->