@charset "utf-8";
/* CSS Document */

/* ************************************************************************************************************************

Comunidad Latina en Australia

File:			app.scss
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* ---------- Variables ---------- */

$primary_color: #5d5d5d;
$secondary_color: #00002f;
$linear_left: #00fcf9;
$linear_right: #0168e4;
$color_sidney: #c0272d;
$color_melbourne: #29aae1;
$color_brisbane: #39b44a;
$color_perth: #ffff00;
$color_adelaide: #2e3191;
$color_gold_coast: #006837;
$color_canberra: #662d90;
$color_darwin: #f6921e;
$color_cairns: #0071bb;
$color_hobart: #ec1e79;
$background: #f1f1f1;
$border: #00002f;

/* ---------- General ---------- */

@import url('https://fonts.googleapis.com/css?family=Open+Sans');

* {
	margin: 0;
	padding: 0;
}

body {
	font: {
		family: 'Open Sans', sans-serif;
		size: 14px;
	}
	color: $primary_color;
}

h1,
h2,
h3,
h4,
h5,
h6 {
	font-family: 'Open Sans', sans-serif;
}

a {
	color: $secondary_color;
	&:hover {
		color: $secondary_color;
	}
}

/* ---------- Template ---------- */

/* Top */

.top {
	background: {
		color: $secondary_color;
		image: url(top_bg.png);
		position: bottom;
		repeat: repeat-x;
	}
	.moduletable_to1 {
		padding: 20px 0;
	}
	.moduletable_to2 {
		.top-bar {
			padding: 0;
			background-color: transparent;
			.top-bar-title {
				margin: 0;
			}
			#responsive-menu {
				width: 100%;
				ul.menu {
					background-color: transparent;
					li {
						padding: 0 5px;
						background: {
							image: url(moduletable_to2_menu_li_bg.png);
							position: right;
							repeat: no-repeat;
						}
						&:last-child {
							background: none;
						}
						&.current-menu-parent,
						&.current-menu-item,
						&.current_page_parent {
							a {
								-moz-border-image: -moz-linear-gradient(left, $linear_left 0%, $linear_right 100%);
								-webkit-border-image: -webkit-linear-gradient(left, $linear_left 0%, $linear_right 100%);
								border-image: linear-gradient(to right, $linear_left 0%, $linear_right 100%);
								border-image-slice: 1;
							}
						}
						a {
							padding: 5px 13px;
							border: 2px solid transparent;
							font-size: 12px;
							color: #fff;
							text-transform: uppercase;
							&:hover {
								-moz-border-image: -moz-linear-gradient(left, $linear_left 0%, $linear_right 100%);
								-webkit-border-image: -webkit-linear-gradient(left, $linear_left 0%, $linear_right 100%);
								border-image: linear-gradient(to right, $linear_left 0%, $linear_right 100%);
								border-image-slice: 1;
							}
						}
					}
				}
			}
		}
	}
	.moduletable_to3 {
		a {
			padding: 5px;
			border: 2px solid transparent;
			-moz-border-image: -moz-linear-gradient(left, $linear_left 0%, $linear_right 100%);
			-webkit-border-image: -webkit-linear-gradient(left, $linear_left 0%, $linear_right 100%);
			border-image: linear-gradient(to right, $linear_left 0%, $linear_right 100%);
			border-image-slice: 1;
			font-size: 12px;
			color: #fff;
			text-transform: uppercase;
			display: inline-block;
		}
	}
	.moduletable_to4 {}
}

.top.sidney,
.top.melbourne,
.top.brisbane,
.top.perth,
.top.adelaide,
.top.gold_coast,
.top.canberra,
.top.darwin,
.top.cairns,
.top.hobart {
	background-image: none;
}

.top.sidney {
	border-bottom: 5px solid $color_sidney;
}

.top.melbourne {
	border-bottom: 5px solid $color_melbourne;
}

.top.brisbane {
	border-bottom: 5px solid $color_brisbane;
}

.top.perth {
	border-bottom: 5px solid $color_perth;
}

.top.adelaide {
	border-bottom: 5px solid $color_adelaide;
}

.top.gold_coast {
	border-bottom: 5px solid $color_gold_coast;
}

.top.canberra {
	border-bottom: 5px solid $color_canberra;
}

.top.darwin {
	border-bottom: 5px solid $color_darwin;
}

.top.cairns {
	border-bottom: 5px solid $color_cairns;
}

.top.hobart {
	border-bottom: 5px solid $color_hobart;
}

/* Search */

.search {
	background: {
		position: center;
		repeat: no-repeat;
		size: cover;
	}
	.moduletable_se1 {
		.logo {
			padding: 90px 0 20px 0;
		}
		.publish {
			margin-top: 10px;
			padding: 20px 0 12px 0;
			background-color: #fbed21;
			-webkit-border-radius: 0 0 10px 10px;
			border-radius: 0 0 10px 10px;
			p {
				margin: 0;
				margin-bottom: 12px;
				font: {
					size: 20px;
					weight: bold;
				}
				color: $color_cairns;
				line-height: 20px;
			}
			a.button {
				margin: 0;
				background-color: $color_cairns;
				border-radius: 0;
				font: {
					size: 16px;
					weight: bold;
				}
			}
		}
		.search {
			margin-bottom: 90px;
			h3 {
				margin-bottom: 10px;
				font-size: 14px;
				color: #fff;
			}
			.search_container {
				padding: 10px;
				background-color: rgba(255, 255, 255, 0.9);
				-webkit-border-radius: 10px 10px 10px 10px;
				border-radius: 10px 10px 10px 10px;
				select {
					margin: 0;
					border-color: $secondary_color;
				}
				.input-group {
					margin: 0;
					border-color: $secondary_color;
					input {
					}
					button.button {
						background-color: $secondary_color;
					}
				}
			}
		}
	}
}

.search.home {
	background-image: url(search_home_bg.png);
}

.search.sidney,
.search.melbourne,
.search.brisbane,
.search.perth,
.search.adelaide,
.search.gold_coast,
.search.canberra,
.search.darwin,
.search.cairns,
.search.hobart {
	.moduletable_se1 {
		.search {
			margin-top: 260px;
		}
	}
}

.search.sidney {
	background-image: url(search_01_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_sidney;
			p {
				color: #fff;
			}
			a.button {
				
			}
		}
	}
}

.search.melbourne {
	background-image: url(search_02_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_melbourne;
			p {
				color: #fff;
			}
			a.button {
				
			}
		}
	}
}

.search.brisbane {
	background-image: url(search_03_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_brisbane;
			p {
				color: #fff;
			}
			a.button {
				
			}
		}
	}
}

.search.perth {
	background-image: url(search_04_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_perth;
			a.button {
				
			}
		}
	}
}

.search.adelaide {
	background-image: url(search_05_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_adelaide;
			p {
				color: #fff;
			}
			a.button {
				
			}
		}
	}
}

.search.gold_coast {
	background-image: url(search_06_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_gold_coast;
			p {
				color: #fff;
			}
			a.button {
				
			}
		}
	}
}

.search.canberra {
	background-image: url(search_01_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_canberra;
			p {
				color: #fff;
			}
			a.button {
				
			}
		}
	}
}

.search.darwin {
	background-image: url(search_02_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_darwin;
			p {
				color: #fff;
			}
			a.button {
				
			}
		}
	}
}

.search.cairns {
	background-image: url(search_03_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_cairns;
			p {
				color: #fff;
			}
			a.button {
				background-color: #fff;
				color: $color_cairns;
			}
		}
	}
}

.search.hobart {
	background-image: url(search_04_bg.png);
	.moduletable_se1 {
		.publish {
			background-color: $color_hobart;
			p {
				color: #fff;
			}
			a.button {
				
			}
		}
	}
}

/* Content */

.content {
	padding: 40px 0;
	h1 {
		font: {
			size: 22px;
			weight: bold;
		}
	}
	h2 {
		font: {
			size: 24px;
			weight: normal;
		}
	}
	a {
		&.button_bg {
			margin-bottom: 10px;
			padding: 70px 0;
			background: {
				color: transparent;
				position: center;
				repeat: no-repeat;
				size: cover;
			}
			border-radius: 0;
			font: {
				size: 22px;
				weight: bold;
			}
		}
		&.button_text {
			margin: 0;
			padding: 15px 0;
			border-radius: 0;
			font: {
				size: 14px;
				weight: bold;
			}
		}
	}
	.asterisk {
		color: red;
	}
	input {
		&.button {
			background-color: $secondary_color;
			color: #fff;
		}
	}
	#sidney {
		a.button_bg {
			background-image: url(button_sidney_bg.png);
		}
		a.button_text {
			background-color: $color_sidney;
		}
	}
	#melbourne {
		a.button_bg {
			background-image: url(button_melbourne_bg.png);
		}
		a.button_text {
			background-color: $color_melbourne;
		}
	}
	#brisbane {
		a.button_bg {
			background-image: url(button_brisbane_bg.png);
		}
		a.button_text {
			background-color: $color_brisbane;
		}
	}
	#perth {
		a.button_bg {
			background-image: url(button_perth_bg.png);
		}
		a.button_text {
			background-color: $color_perth;
			color: $secondary_color;
		}
	}
	#adelaide {
		a.button_bg {
			background-image: url(button_adelaide_bg.png);
		}
		a.button_text {
			background-color: $color_adelaide;
		}
	}
	#gold_coast {
		a.button_bg {
			background-image: url(button_gold_coast_bg.png);
		}
		a.button_text {
			background-color: $color_gold_coast;
		}
	}
	#canberra {
		a.button_bg {
			background-image: url(button_canberra_bg.png);
		}
		a.button_text {
			background-color: $color_canberra;
		}
	}
	#darwin {
		a.button_bg {
			background-image: url(button_darwin_bg.png);
		}
		a.button_text {
			background-color: $color_darwin;
		}
	}
	#cairns {
		a.button_bg {
			background-image: url(button_cairns_bg.png);
		}
		a.button_text {
			background-color: $color_cairns;
		}
	}
	#hobart {
		a.button_bg {
			background-image: url(button_hobart_bg.png);
		}
		a.button_text {
			background-color: $color_hobart;
		}
	}
}

#main_categories {
	a {
		&.image {
			width: 130px;
			height: 130px;
			background: {
				position: center;
				repeat: no-repeat;
			}
			display: inline-block;
		}
		&.text {
			margin-bottom: 10px;
			font-size: 12px;
			text-transform: uppercase;
			display: block;
		}
	}
	.asesorias {
		a {
			&.image {
				background-image: url('category_asesorias.png');
				&:hover {
					background-image: url('category_asesorias_hover.png');
				}
			}
		}
	}
	.belleza_y_cuidado_del_cuerpo {
		a {
			&.image {
				background-image: url('category_belleza_y_cuidado_del_cuerpo.png');
				&:hover {
					background-image: url('category_belleza_y_cuidado_del_cuerpo_hover.png');
				}
			}
		}
	}
	.servicios_para_hogar {
		a {
			&.image {
				background-image: url('category_servicios_para_hogar.png');
				&:hover {
					background-image: url('category_servicios_para_hogar_hover.png');
				}
			}
		}
	}
	.educacion {
		a {
			&.image {
				background-image: url('category_educacion.png');
				&:hover {
					background-image: url('category_educacion_hover.png');
				}
			}
		}
	}
	.vehiculos {
		a {
			&.image {
				background-image: url('category_vehiculos.png');
				&:hover {
					background-image: url('category_vehiculos_hover.png');
				}
			}
		}
	}
	.motos {
		a {
			&.image {
				background-image: url('category_motos.png');
				&:hover {
					background-image: url('category_motos_hover.png');
				}
			}
		}
	}
	.bicicletas {
		a {
			&.image {
				background-image: url('category_bicicletas.png');
				&:hover {
					background-image: url('category_bicicletas_hover.png');
				}
			}
		}
	}
	.alimentos_y_bebidas {
		a {
			&.image {
				background-image: url('category_alimentos_y_bebidas.png');
				&:hover {
					background-image: url('category_alimentos_y_bebidas_hover.png');
				}
			}
		}
	}
	.alojamiento {
		a {
			&.image {
				background-image: url('category_alojamiento.png');
				&:hover {
					background-image: url('category_alojamiento_hover.png');
				}
			}
		}
	}
	.marketing_publicidad_y_diseno_grafico {
		a {
			&.image {
				background-image: url('category_marketing_publicidad_y_diseno_grafico.png');
				&:hover {
					background-image: url('category_marketing_publicidad_y_diseno_grafico_hover.png');
				}
			}
		}
	}
	.video_y_fotografia {
		a {
			&.image {
				background-image: url('category_video_y_fotografia.png');
				&:hover {
					background-image: url('category_video_y_fotografia_hover.png');
				}
			}
		}
	}
	.deportes_y_recreacion {
		a {
			&.image {
				background-image: url('category_deportes_y_recreacion.png');
				&:hover {
					background-image: url('category_deportes_y_recreacion_hover.png');
				}
			}
		}
	}
	.salud {
		a {
			&.image {
				background-image: url('category_salud.png');
				&:hover {
					background-image: url('category_salud_hover.png');
				}
			}
		}
	}
	.trabajo {
		a {
			&.image {
				background-image: url('category_trabajo.png');
				&:hover {
					background-image: url('category_trabajo_hover.png');
				}
			}
		}
	}
	.transporte_y_logistica {
		a {
			&.image {
				background-image: url('category_transporte_y_logistica.png');
				&:hover {
					background-image: url('category_transporte_y_logistica_hover.png');
				}
			}
		}
	}
	.diversion {
		a {
			&.image {
				background-image: url('category_diversion.png');
				&:hover {
					background-image: url('category_diversion_hover.png');
				}
			}
		}
	}
	.turismo_y_viajes {
		a {
			&.image {
				background-image: url('category_turismo_y_viajes.png');
				&:hover {
					background-image: url('category_turismo_y_viajes_hover.png');
				}
			}
		}
	}
	.ropa_zapatos_y_accesorios {
		a {
			&.image {
				background-image: url('category_ropa_zapatos_y_accesorios.png');
				&:hover {
					background-image: url('category_ropa_zapatos_y_accesorios_hover.png');
				}
			}
		}
	}
	.artesanias_y_suvenires {
		a {
			&.image {
				background-image: url('category_artesanias_y_suvenires.png');
				&:hover {
					background-image: url('category_artesanias_y_suvenires_hover.png');
				}
			}
		}
	}
	.mascotas {
		a {
			&.image {
				background-image: url('category_mascotas.png');
				&:hover {
					background-image: url('category_mascotas_hover.png');
				}
			}
		}
	}
	.cultura_arte_y_religion {
		a {
			&.image {
				background-image: url('category_cultura_arte_y_religion.png');
				&:hover {
					background-image: url('category_cultura_arte_y_religion_hover.png');
				}
			}
		}
	}
	.regalos {
		a {
			&.image {
				background-image: url('category_regalos.png');
				&:hover {
					background-image: url('category_regalos_hover.png');
				}
			}
		}
	}
	.bancos_y_cambios_de_moneda {
		a {
			&.image {
				background-image: url('category_bancos_y_cambios_de_moneda.png');
				&:hover {
					background-image: url('category_bancos_y_cambios_de_moneda_hover.png');
				}
			}
		}
	}
}

/* Block 1 */

.block_1 {
	.moduletable_b11 {}
	.moduletable_b12 {
		h3 {
			margin-bottom: 40px;
			background: {
				image: url(moduletable_b12_h3_bg.png);
				position: center;
				repeat: no-repeat;
			}
			font: {
				size: 18px;
				weight: bold;
			}
			text: {
				align: center;
				transform: uppercase;
			}
		}
		.upw-posts {
			display: flex;
			flex-direction: row;
			align-items: flex-start;
			article {
				border-bottom: 0;
				header {
					div.entry-image {
						text-align: center;
					}
					h4.entry-title {
						font: {
							size: 20px;
							weight: bold;
						}
						text: {
							align: center;
							transform: uppercase;
						}
					}
				}
				div.entry-summary {
					padding: 0 10px;
					a.more-link {
						max-width: 100px;
						margin: 10px auto;
						border: 1px solid $secondary_color;
						text: {
							align: center;
							transform: uppercase;
						}
						display: inherit;
					}
				}
			}
		}
	}
	.moduletable_b13 {
		h3 {
			margin-bottom: 15px;
			padding-bottom: 15px;
			background: {
				image: url(moduletable_b13_h3_bg.png);
				position: bottom;
				repeat: no-repeat;
			}
			font: {
				size: 18px;
				weight: bold;
			}
			text: {
				align: center;
				transform: uppercase;
			}
		}
		.g.g-1 {
			margin-bottom: 10px;
		}
	}
}

/* Newsletter */

.newsletter {
	padding: 30px 0;
	background-color: $background;
	.moduletable_n1 {
		h2 {
			width: 40%;
			margin: 0;
			font-size: 18px;
			text-align: center;
			float: left;
			position: relative;
		}
		.tnp-widget {
			width: 60%;
			float: left;
			position: relative;
			.tnp-field {
				margin: 0;
			}
			.tnp-field-email {
				width: 60%;
				float: left;
				position: relative;
				label {
					display: none;
				}
				input.tnp-email {
					margin: 0;
					background-color: #fff;
				}
			}
			.tnp-field-button {
				width: 30%;
				float: left;
				position: relative;
				input.tnp-submit {
					margin: 0;
					background-color: $secondary_color;
					border-radius: 0;
				}
			}
		}
	}
}

/* Social Media */

.social_media {
	padding: 30px 0;
	.moduletable_sm1 {
		#a {
			a.button {
				margin: 0;
				padding: 15px 35px;
				border: 2px solid transparent;
				-moz-border-image: -moz-linear-gradient(left, $linear_left 0%, $linear_right 100%);
				-webkit-border-image: -webkit-linear-gradient(left, $linear_left 0%, $linear_right 100%);
				border-image: linear-gradient(to right, $linear_left 0%, $linear_right 100%);
				border-image-slice: 1;
				font: {
					size: 20px;
					weight: bold;
				}
				color: $secondary_color;
			}
		}
		#b {
			a {
				margin: 0 10px;
				display: inline-block;
			}
		}
	}
}

/* Bottom */

.bottom {
	background-color: $secondary_color;
	.moduletable_bo1,
	.moduletable_bo2 {
		color: #fff;
		a {
			color: #fff;
		}
	}
	.moduletable_bo1 {
		padding: 30px 0;
		ul.menu {
			li {
				background: {
					image: url(moduletable_bo1_menu_li_bg.png);
					position: right;
					repeat: no-repeat;
				}
				&:last-child {
					background: none;
				}
				a {
					padding: 5px 15px;
					text-transform: uppercase;
				}
			}
		}
	}
	.moduletable_bo2 {
		font-size: 20px;
		i {
			margin: 0 15px;
		}
	}
}

/* Copyright */

.copyright {
	padding: 10px 0;
	background-color: $secondary_color;
	color: #fff;
	a {
		color: #fff;
	}
}

/* Back Home */

.back_home {
	padding: 10px;
	top: 30%;
	left: 0;
	background-color: $secondary_color;
	position: fixed;
	a {
		color: #fff;
		text-transform: uppercase;
		writing-mode: vertical-lr;
		transform: rotate(180deg);
		i {
			transform: rotate(90deg);
		}
	}
}

/* ---------- Media Queries ---------- */

/* Small only (639px) */

@media screen and (max-width: 39.9375em) {}

/* Medium and up (640px) */

@media screen and (min-width: 40em) {}

/* Medium only (640px and 1023px) */

@media screen and (min-width: 40em) and (max-width: 63.9375em) {}

/* Large and up (1024px) */

@media screen and (min-width: 64em) {}

/* Large only (1024px and 1199px) */

@media screen and (min-width: 64em) and (max-width: 74.9375em) {}

/* ---------- Others ---------- */

.clear {
	clear: both;
}